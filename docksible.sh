#!/bin/bash

ENTRYPOINT=${0##*/}
IMAGE=${DOCKSIBLE_IMAGE:-ansible}
IMAGE_TAG=${DOCKSIBLE_IMAGE_TAG:-latest}
WORKDIR=${DOCKSIBLE_WORKDIR:-${PWD}}
WORKDIR_READONLY=${DOCKSIBLE_WORKDIR_READONLY:-true}
EXTRA_VOLUMES=${DOCKSIBLE_EXTRA_VOLUMES}
HOME_VOLUME=${DOCKSIBLE_HOME_VOLUME}
SSH_DIR=${DOCKSIBLE_SSH_DIR}
DOCKER_RUN_OPTS=${DOCKSIBLE_DOCKER_RUN_OPTS:---rm -ti --read-only}

[[ $DOCKSIBLE_DEBUG != true ]] && unset DOCKSIBLE_DEBUG

debug() {
    if [[ $DOCKSIBLE_DEBUG = true ]]; then
        echo "[docksible]: $@"
    fi
}

add_volume() {
    local vol=$1
    debug "add volume $vol"
    VOLUMES="$VOLUMES --mount $vol"
}

add_env() {
    local env=$1
    set ${env/=/ }
    local var=$1
    local val=$2

    if [[ -z "$val" ]]; then
        val=$(eval echo \$$env)
    fi

    debug "add variable $var=$val"
    ENVIRONMENT="$ENVIRONMENT -e $env"
}

set_ansible_cfg() {
    local cfg

    if [[ "$ANSIBLE_CONFIG" ]]; then
        cfg=$ANSIBLE_CONFIG
    elif [[ -f ansible.cfg ]]; then
        cfg=ansible.cfg
    elif [[ -f $HOME/.ansible.cfg ]]; then
        cfg=$HOME/.ansible.cfg
    elif [[ -f /etc/ansible/ansible.cfg ]]; then
        cfg=/etc/ansible/ansible.cfg
    fi

    if [[ "$cfg" ]]; then
        if [[ -f $cfg ]]; then
            add_volume type=bind,src=$(realpath $cfg),dst=/etc/ansible/ansible.cfg,readonly
        else
            echo "Config file '$cfg' set but file not found" >&2
            exit 1
        fi
    else
        debug "ansible config file not set"
    fi
}

set_ansible_env() {
    local var
    for var in ${!ANSIBLE_*}; do
        add_env $var
    done
}

add_extra_volumes() {
    local voldef vol opts
    for voldef in $EXTRA_VOLUMES; do
        set ${voldef/,/ }
        vol=$1
        opts=$2
        add_volume type=bind,src=$vol,dst=${vol}${opts:+,$opts}
    done
}

volume_cleanup() {
    debug remove /root volume $(docker volume rm $HOME_VOLUME)
}

# main()
set_ansible_cfg
set_ansible_env
add_volume type=tmpfs,dst=/tmp

if [[ "$SSH_AUTH_SOCK" ]]; then
    add_volume type=bind,src=$SSH_AUTH_SOCK,dst=/ssh_auth_sock,readonly
    add_env SSH_AUTH_SOCK=/ssh_auth_sock
fi

if [[ "$HOME_VOLUME" ]]; then
    add_volume type=volume,src=$HOME_VOLUME,dst=/root
elif [[ -z "$HOME_VOLUME" ]] && [[ "$SSH_DIR" ]]; then
    HOME_VOLUME=$(docker volume create)
    trap volume_cleanup EXIT
    add_volume type=volume,src=$HOME_VOLUME,dst=/root
else
    add_volume type=tmpfs,dst=/root
fi

if [[ "$SSH_DIR" ]]; then
    debug "set up /root/.ssh"
    docker run --rm -it --entrypoint "" \
        --mount type=bind,src=$SSH_DIR,dst=$SSH_DIR,readonly \
        --mount type=volume,src=$HOME_VOLUME,dst=/root \
        $IMAGE:$IMAGE_TAG \
        bash -c "rsync ${DOCKSIBLE_DEBUG:+-v} -r --delete $SSH_DIR/ /root/.ssh/ && chown -R root: /root/.ssh"
fi

if [[ "$WORKDIR" ]]; then
    [[ "$WORKDIR_READONLY" == true ]] && WORKDIR_MNT_OPTS=readonly
    add_volume type=bind,src=$WORKDIR,dst=${WORKDIR}${WORKDIR_MNT_OPTS:+,$WORKDIR_MNT_OPTS}
fi

if [[ "$EXTRA_VOLUMES" ]]; then
    add_extra_volumes
fi

debug docker run $DOCKER_RUN_OPTS $VOLUMES $ENVIRONMENT ${WORKDIR:+-w $WORKDIR} --entrypoint $ENTRYPOINT $IMAGE:$IMAGE_TAG "$@"
docker run $DOCKER_RUN_OPTS $VOLUMES $ENVIRONMENT ${WORKDIR:+-w $WORKDIR} --entrypoint $ENTRYPOINT $IMAGE:$IMAGE_TAG "$@"
