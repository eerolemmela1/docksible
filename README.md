# docksible

Run dockerized ansible commands like it was installed locally.

<!-- vim-markdown-toc GitLab -->

* [Quickstart](#quickstart)
* [Configuration](#configuration)
  * [Docksible configuration](#docksible-configuration)
  * [Ansible configuration](#ansible-configuration)

<!-- vim-markdown-toc -->

## Quickstart

* create a docker image with the desired version of Ansible (see my
    [docker/ansible](https://gitlab.com/eerolemmela1/docker/ansible) for an example)
* `docksible` detects the desired Ansible command by the name it is called by,
    so create symlinks pointing to it or a set of aliases like:

```bash
alias ansible=/path/to/docksible.sh
alias ansible-config=/path/to/docksible.sh
alias ansible-connection=/path/to/docksible.sh
alias ansible-console=/path/to/docksible.sh
alias ansible-doc=/path/to/docksible.sh
alias ansible-galaxy=/path/to/docksible.sh
alias ansible-inventory=/path/to/docksible.sh
alias ansible-lint=/path/to/docksible.sh
alias ansible-playbook=/path/to/docksible.sh
alias ansible-pull=/path/to/docksible.sh
alias ansible-test=/path/to/docksible.sh
alias ansible-vault=/path/to/docksible.sh
```

* start ssh-agent and add the keys needed (`docksible` mounts `$SSH_AUTH_SOCK`
    in to the container)
* `cd` to a directory where you keep your playbooks and inventory and run the
    ansible commands that you are familiar with

## Configuration

### Docksible configuration

The behavior of `docksible` can be modified with the environment variables in
the table below.

| Environment variable         | Default                | Description                                                                 |
|------------------------------|------------------------|-----------------------------------------------------------------------------|
| `DOCKSIBLE_IMAGE`            | `ansible`              | The ansible docker image name without image tag                             |
| `DOCKSIBLE_IMAGE_TAG`        | `latest`               | The ansible docker image tag                                                |
| `DOCKSIBLE_WORKDIR`          | `${PWD}`               | Bind mount dir from host to container and set as working directory          |
| `DOCKSIBLE_WORKDIR_READONLY` | `true`                 | Mount working directory read only by default                                |
| `DOCKSIBLE_EXTRA_VOLUMES`    |                        | Any extra bind mounts with mount options, eg. `~/my_ansible_roles,readonly` |
| `DOCKSIBLE_HOME_VOLUME`      |                        | Docker volume name for persisting caches, .ssh files etc.                   |
| `DOCKSIBLE_SSH_DIR`          |                        | If set, contents is copied to `~/.ssh` in the container                     |
| `DOCKSIBLE_DOCKER_RUN_OPTS`  | `--rm -ti --read-only` | Options that are passed to `docker run`                                     |
| `DOCKSIBLE_DEBUG`            |  `false`               | Set to `true` for debug output                                              |

### Ansible configuration

> Changes can be made and used in a configuration file which will be searched
> for in the following order:
>
> * ANSIBLE_CONFIG (environment variable if set)
> * ansible.cfg (in the current directory)
> * ~/.ansible.cfg (in the home directory)
> * /etc/ansible/ansible.cfg
>
> Ansible will process the above list and use the first file found, all others
> are ignored.

*<https://docs.ansible.com/ansible/latest/reference_appendices/config.html>*

`docksible` also processes the above list and mounts the first file found in to
the container as `/etc/ansible/ansible.cfg`. As Ansible can be configured with
environment variables too, all variables in users environment that start with
`ANSIBLE_` are exported to the containers environment.
